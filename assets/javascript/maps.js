import styles from './maps/styles';

var map;

function initMap() {
    const coords = {
        lat: -13.515138979878401,
        lng: -71.98771890383098
    };

    map = new google.maps.Map(document.getElementById('mapa'), {
        center: coords,
        zoom: 19,
        styles: styles
    });

    let marker = new google.maps.Marker({
        position: coords,
        map,
        title: 'IO Map'
    })

}

initMap();