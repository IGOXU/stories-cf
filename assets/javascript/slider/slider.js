export default class Slider {
    constructor({ elements, animationFunction, speed = 5000 }) {
        this.elements = elements;
        this.animationFunction = animationFunction;
        this.speed = speed;

        this.index = 0;
        this.size = elements.length;

        this.innerPrev = this.innerPrev.bind(this);
        this.innerNext = this.innerNext.bind(this);

        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);
        this.stop = this.stop.bind(this);
    }

    innerNext() {
        this.index++;
        if (this.index >= this.size) this.index = 0;

        this.animationFunction(this.elements[this.index]);
    }

    innerPrev() {
        this.index--;
        if (this.index < 0) this.index = this.size - 1;

        this.animationFunction(this.elements[this.index]);
    }

    next() {
        this.innerNext();
        if (this.interval) {
            this.stop();
            this.play();
        }
    }

    prev() {
        this.innerPrev();
        if (this.interval) {
            this.stop();
            this.play();
        }
    }

    play() {
        this.interval = setInterval(this.next, this.speed);
    }

    stop() {
        clearInterval(this.interval);
    }
}