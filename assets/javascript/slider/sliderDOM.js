import Slider from './slider';
import elements from './elements'
import Preloader from '../preloader/preloader';

let sliderSubtitle = document.querySelector("#slider-subtitle");
let sliderTitle = document.querySelector("#slider-title");
let sliderText = document.querySelector("#slider-text");
let sliderImage = document.querySelector("#slider-image");
let textContent = document.querySelector("#slider-text-content");

let leftArrow = document.querySelector(".left-arrow");
let rightArrow = document.querySelector(".right-arrow");

let slider = new Slider({
    elements,
    animationFunction: (element) => {
        textContent.classList.add("hide");
        sliderImage.classList.add("hide");

        setTimeout(() => {
            sliderSubtitle.innerHTML = element.subtitle;
            sliderTitle.innerHTML = element.title;
            sliderText.innerHTML = element.text;
            sliderImage.src = element.image;

            textContent.classList.remove("hide");
            sliderImage.classList.remove("hide");
        }, 600);

    },
    speed: 2000
});

slider.play();

leftArrow.addEventListener('click', slider.prev)
rightArrow.addEventListener('click', slider.next)

const imagePaths = elements.map(element => element.image);

Preloader.preloadImages({
    images: imagePaths,
    completed: function() {
        document.querySelector('.controls').style.display = 'block';
    }
})