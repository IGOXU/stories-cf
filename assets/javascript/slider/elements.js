let elements = [{
        title: 'Lorem',
        subtitle: 'Lorem Ipsum',
        image: '../public/images/1.jpg',
        text: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Obcaecati dolorum necessitatibus ab explicabo sequi eaque cum suscipit, laborum adipisci et? Exercitationem fugiat a delectus officiis labore iusto corrupti excepturi atque.'
    },
    {
        title: 'Lorem 2',
        subtitle: 'Lorem Ipsum 2',
        image: '../public/images/3.jpg',
        text: 'Sit amet consectetur adipisicing elit. Obcaecati dolorum necessitatibus ab explicabo sequi eaque cum suscipit, laborum adipisci et? Exercitationem fugiat a delectus officiis labore iusto corrupti excepturi atque.'
    },
    {
        title: 'Lorem 3',
        subtitle: 'Lorem Ipsum 3',
        image: '../public/images/blank-sheet-of-paper-with-ballpoint-and-coffee-cup.jpg',
        text: 'Ipsum, sit amet consectetur adipisicing elit. Obcaecati dolorum necessitatibus ab explicabo sequi eaque cum suscipit, laborum adipisci et? Exercitationem fugiat a delectus officiis labore iusto corrupti excepturi atque.'
    },
    {
        title: 'Lorem 4',
        subtitle: 'Lorem Ipsum 4',
        image: '../public/images/blank-sheet-of-paper-with-ballpoint-and-mobile-phone.jpg',
        text: 'Amet consectetur adipisicing elit. Obcaecati dolorum necessitatibus ab explicabo sequi eaque cum suscipit, laborum adipisci et? Exercitationem fugiat a delectus officiis labore iusto corrupti excepturi atque.'
    },
]

export default elements;