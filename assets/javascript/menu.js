function scrollToElelement(element) {
    window.scrollTo({
        'behavior': 'smooth',
        'top': element.offsetTop
    })
}

document.querySelector(".menu")
    .addEventListener('click', () => {
        document.querySelector('.menu-screen').classList.add('active');
    })


document.querySelector(".close")
    .addEventListener('click', () => {
        document.querySelector('.menu-screen').classList.remove('active');
    })

let links = document.querySelectorAll('.menu-screen a')

links.forEach(link => {
    link.addEventListener('click', function(e) {
        e.preventDefault()
        document.querySelector('.menu-screen').classList.remove('active');

        let path = this.href.split("/")
        let selector = path[path.length - 1]

        if (window.scrollTo) e.preventDefault()

        scrollToElelement(document.querySelector(selector))

        return !!window.scrollTo;
    })
})