const common = require('./webpack.common.js');
const merge = require('webpack-merge');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const devMode = process.env.NODE_ENV == 'production';

module.exports = merge(common, {
    mode: 'production',
    output: {
        publicPath: '.'
    },
    module: {
        rules: [{
                test: /\.(sa|sc|c)ss$/,
                use: [{
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: devMode,
                        },
                    },
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.html$/i,
                loader: 'html-loader',
                options: {
                    attributes: false,
                    minimize: {
                        removeComments: true,
                        collapseWhitespace: false,
                    }
                },
            }
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: devMode ? 'dist/css/[name].css' : 'dist/css/[name].[hash].css',
            chunkFilename: devMode ? 'dist/css/[id].css' : 'dist/css/[id].[hash].css',
            allChunks: true,
        }),
    ],
});